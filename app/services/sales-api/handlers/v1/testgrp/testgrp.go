package testgrp

import (
	"context"
	"encoding/json"
	"errors"
	"github.com/jmoiron/sqlx"
	v1 "gitlab.com/genson/go-service/business/web/v1"
	"go.uber.org/zap"
	"math/rand"
	"net/http"
)

// Handlers manages the set of test endpoints
type Handlers struct {
	Log *zap.SugaredLogger
	DB  *sqlx.DB
}

func (h Handlers) Test(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	if n := rand.Intn(100); n%2 == 0 {

		panic("test panic")
		return v1.NewRequestError(errors.New("failed demo"), http.StatusBadRequest)
		//return web.NewShutdownError("test shutdown")
	}

	status := struct {
		Status string `json:"status"`
	}{
		Status: "OK",
	}

	json.NewEncoder(w).Encode(status)

	h.Log.Infow("Test", "status", http.StatusOK, "method", r.Method, "path", r.URL.Path, "remoteAddr", r.RemoteAddr)
	return nil
}
