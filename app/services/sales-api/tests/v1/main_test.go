package v1

import (
	"fmt"
	"gitlab.com/genson/go-service/business/data/dbtest"
	"gitlab.com/genson/go-service/foundation/docker"
	"testing"
)

var c *docker.Container

func TestMain(m *testing.M) {
	var err error
	c, err = dbtest.StartDB()
	if err != nil {
		fmt.Println(err)
		return
	}
	defer dbtest.StopDB(c)

	m.Run()
}
