package config

import "time"

type Config struct {
	Version struct {
		Build string
		Desc  string
	}
	Web struct {
		APIHost         string
		DebugHost       string
		ReadTimeout     time.Duration
		WriteTimeout    time.Duration
		IdleTimeout     time.Duration
		ShutdownTimeout time.Duration
	}
	Auth struct {
		KeysFolder string
		ActiveKID  string
	}
	DB struct {
		User         string
		Password     string
		Host         string
		Name         string
		MaxIdleConns int
		MaxOpenConns int
		DisableTLS   bool
	}
	Zipkin struct {
		ReporterURI string
		ServiceName string
		Probability float64
	}
	Jaeger struct {
		ReporterURI string
		ServiceName string
		Probability float64
	}
}

func (c *Config) Standard() {
	c.Web.ReadTimeout = c.Web.ReadTimeout * time.Second
	c.Web.WriteTimeout = c.Web.WriteTimeout * time.Second
	c.Web.IdleTimeout = c.Web.IdleTimeout * time.Second
	c.Web.ShutdownTimeout = c.Web.ShutdownTimeout * time.Second
}
