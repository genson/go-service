package commands

import (
	"errors"
	"fmt"
	"gitlab.com/genson/go-service/business/data/dbschema"
	"gitlab.com/genson/go-service/business/sys/database"
)

// ErrHelp provides context that help was given.
var ErrHelp = errors.New("provided help")

func Migrate(cfg database.Config) error {
	db, err := database.Open(cfg)
	if err != nil {
		return fmt.Errorf("connect database: %w", err)
	}
	defer db.Close()

	err = dbschema.Migrate(db)
	if err != nil {
		return err
	}
	fmt.Println("migrations complete")

	return nil
}
