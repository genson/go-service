package config

import (
	"fmt"
	"github.com/spf13/viper"
)

type Config interface {
	Standard()
}

// LoadConfig load config file from given path
func LoadConfig(path string, dest Config) (*viper.Viper, error) {
	v := viper.New()

	v.SetConfigName(path)
	v.AddConfigPath(".")

	err := viper.ReadInConfig()
	if err != nil {
		fmt.Println(err)
	}
	viper.AutomaticEnv()
	if err := v.ReadInConfig(); err != nil {
		return nil, err
	}

	err = v.Unmarshal(dest)
	if err != nil {
		return nil, fmt.Errorf("unable to decode into dest, %v", err)
	}

	dest.Standard()

	return v, nil
}
