package dbschema

import (
	"context"
	"embed"
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/pressly/goose/v3"
	"gitlab.com/genson/go-service/business/sys/database"
)

var (

	//go:embed sql/migrations/*.sql
	schemaDoc embed.FS

	//go:embed sql/seed.sql
	seedDoc string

	//go:embed sql/delete.sql
	deletaDoc string
)

// Migrate attempts to bring the schema for db up to date with the migrations
// defined in this package.
func Migrate(db *sqlx.DB) error {
	goose.SetBaseFS(schemaDoc)

	if err := goose.Up(db.DB, "sql/migrations"); err != nil {
		err = goose.Down(db.DB, "sql/migrations")
		return fmt.Errorf("migration-up: %w", err)
	}

	return nil
}

// Seed runs the set of seed-data queries against db. The queries are ran in a
// transaction and rolled back if any fail.
func Seed(ctx context.Context, db *sqlx.DB) error {
	if err := database.StatusCheck(ctx, db); err != nil {
		return fmt.Errorf("status check database: %w", err)
	}

	tx, err := db.Begin()
	if err != nil {
		return err
	}

	if _, err := tx.Exec(seedDoc); err != nil {
		if err := tx.Rollback(); err != nil {
			return err
		}
		return err
	}

	return tx.Commit()
}

// DeleteAll runs the set of Drop-table queries against db. The queries are ran in a
// transaction and rolled back if any fail.
func DeleteAll(db *sqlx.DB) error {
	tx, err := db.Beginx()
	if err != nil {
		return err
	}

	if _, err := tx.Exec(deletaDoc); err != nil {
		if err = tx.Rollback(); err != nil {
			return err
		}
		return err
	}

	return tx.Commit()
}
