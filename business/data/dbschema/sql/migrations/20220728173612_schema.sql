-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS users (
    user_id       UUID,
    name          TEXT,
    email         TEXT UNIQUE,
    roles         TEXT[],
    password_hash TEXT,
    date_created  TIMESTAMP,
    date_updated  TIMESTAMP,

    PRIMARY KEY (user_id)
    );

CREATE TABLE IF NOT EXISTS products (
    product_id   UUID,
    name         TEXT,
    cost         INT,
    quantity     INT,
    user_id      UUID,
    date_created TIMESTAMP,
    date_updated TIMESTAMP,

    PRIMARY KEY (product_id),
    FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE
    );

CREATE TABLE IF NOT EXISTS sales (
    sale_id      UUID,
    user_id      UUID,
    product_id   UUID,
    quantity     INT,
    paid         INT,
    date_created TIMESTAMP,

    PRIMARY KEY (sale_id),
    FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE,
    FOREIGN KEY (product_id) REFERENCES products(product_id) ON DELETE CASCADE
    );
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS sales;

DROP TABLE IF EXISTS users;

DROP TABLE IF EXISTS products;
-- +goose StatementEnd
