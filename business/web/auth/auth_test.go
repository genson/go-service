package auth_test

import (
	"crypto/rand"
	"crypto/rsa"
	"github.com/golang-jwt/jwt/v4"
	"gitlab.com/genson/go-service/business/web/auth"
	"testing"
	"time"
)

// Success and failure markers.
const (
	success = "\u2713"
	failed  = "\u2717"
)

// Colors
const (
	colorReset   = "\033[0m"
	failedColor  = "\033[31m"
	successColor = "\033[32m"
	ColorBlue    = "\033[34m"
)

func TestAuth(t *testing.T) {
	t.Log("Given the need to be able to authenticate and authorize access.")
	{
		testID := 0
		t.Logf("\tTest %d:\tWhen handling a single user.", testID)
		{
			const keyID = "54bb2165-71e1-41a6-af3e-7da4a0e1e2c1"
			privateKey, err := rsa.GenerateKey(rand.Reader, 2048)
			if err != nil {
				t.Fatalf("%s\t%s\tTest %d:\tShould be able to create a private key: %v", failedColor, failed, testID, err)
			}
			t.Logf("%s\t%s\tTest %d:\tShould be able to create a private key.", successColor, success, testID)

			a, err := auth.New(keyID, &keyStore{privateKey})
			if err != nil {
				t.Fatalf("%s\t%s\tTest %d:\tShould be able to create an authenticator: %v", failedColor, failed, testID, err)
			}
			t.Logf("%s\t%s\tTest %d:\tShould be able to create an authenticator.", successColor, success, testID)

			claims := auth.Claims{
				RegisteredClaims: jwt.RegisteredClaims{
					Issuer:    "service project",
					Subject:   "54bb2165-71e1-41a6-af3e-7da4a0e1e23hi",
					ExpiresAt: jwt.NewNumericDate(time.Now().UTC().Add(time.Hour)),
					IssuedAt:  jwt.NewNumericDate(time.Now().UTC()),
				},
				Roles: []string{auth.RoleAdmin},
			}

			token, err := a.GenerateToken(claims)
			if err != nil {
				t.Fatalf("%s\t%s\tTest %d:\tShould be able to generate a JWT: %v", failedColor, failed, testID, err)
			}
			t.Logf("%s\t%s\tTest %d:\tShould be able to generate a JWT.", successColor, success, testID)

			parsedClaims, err := a.ValidateToken(token)
			if err != nil {
				t.Fatalf("%s\t%s\tTest %d:\tShould be able to parse the claims: %v", failedColor, failed, testID, err)
			}
			t.Logf("%s\t%s\tTest %d:\tShould be able to parse a claims.", successColor, success, testID)

			if exp, got := len(claims.Roles), len(parsedClaims.Roles); exp != got {
				t.Logf("%s\t\tTest %d:\texp: %d", ColorBlue, testID, exp)
				t.Logf("%s\t\tTest %d:\tgot: %d", ColorBlue, testID, got)
				t.Fatalf("%s\t%s\tTest %d:\tShould have the expected number of roles.", failedColor, failed, testID)
			}
			t.Logf("%s\t%s\tTest %d:\tShould have the expected number of roles.", successColor, success, testID)

		}
		t.Logf("%s", colorReset)
	}
}

// =====================================================================================================================

type keyStore struct {
	pk *rsa.PrivateKey
}

func (ks *keyStore) PrivateKey(kid string) (*rsa.PrivateKey, error) {
	return ks.pk, nil
}

func (ks *keyStore) PublicKey(kid string) (*rsa.PublicKey, error) {
	return &ks.pk.PublicKey, nil
}
